package com.example.diceroller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rollButton: Button = findViewById(R.id.button)
        rollButton.setOnClickListener {
            rollDice()
        }
    }

    private fun rollDice() {
        val dice = Dice(6)
        val diceRoll = dice.roll()
        val diceRoll2 = dice.roll()
        changeDice(2, diceRoll)
        changeDice(1, diceRoll2)
    }

    private fun changeDice(dice : Int, diceSide : Int) {
        var diceImage : ImageView = findViewById<ImageView>(R.id.imageView2)
        if (dice == 2) {
            var diceImage = findViewById<ImageView>(R.id.imageView2)
            when (diceSide) {
                1 -> {
                    diceImage.setImageResource(R.drawable.dice_1)
                }
                2 -> {
                    diceImage.setImageResource(R.drawable.dice_2)
                }
                3 -> {
                    diceImage.setImageResource(R.drawable.dice_3)
                }
                4 -> {
                    diceImage.setImageResource(R.drawable.dice_4)
                }
                5 -> {
                    diceImage.setImageResource(R.drawable.dice_5)
                }
                6 -> {
                    diceImage.setImageResource(R.drawable.dice_6)
                }
            }
        }
        else if (dice == 1) {
            var diceImage = findViewById<ImageView>(R.id.imageView)
            when (diceSide) {
                1 -> {
                    diceImage.setImageResource(R.drawable.dice_1)
                }
                2 -> {
                    diceImage.setImageResource(R.drawable.dice_2)
                }
                3 -> {
                    diceImage.setImageResource(R.drawable.dice_3)
                }
                4 -> {
                    diceImage.setImageResource(R.drawable.dice_4)
                }
                5 -> {
                    diceImage.setImageResource(R.drawable.dice_5)
                }
                6 -> {
                    diceImage.setImageResource(R.drawable.dice_6)
                }
            }
        }
    }
}

class Dice(private val numSides : Int) {
    fun roll(): Int{
        return (1..numSides).random()
    }
}